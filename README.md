# Installation de Arch Linux sur un Asus Rog Strix Scar II, avec environnement Cinnamon et /home séparée.

- La partition `/` se trouve sur `/dev/nvme0n1p6`
- La partition `/home` se trouve sur `/dev/sda3`
- La partition `ef`i se trouve sur `/dev/nvme0n1p1`

## Disposition du clavier :
    
    loadkeys fr-pc # /!\ N'est pas équivalent au jeu de clés fr tout court !

## Connexion au Wi-Fi :

    wifi-menu

## Montage des partitions :

    cfdisk #Vérification des partitions de /dev/sda (par défaut)
    cfdisk /dev/nvme0n1 #Vérification des partitions de /dev/nvme0n1

    mkdir /mnt/{arch,home}
    mount /dev/nme0n1p6 /mnt/arch
    mount /dev/sda3 /mnt/home

## Installation du système de base :

    pacstrap /mnt/arch base base-devel
    genfstab -U -p /mnt/arch >> /mnt/arch/etc/fstab
    genfstab -U -p /mnt/home >> /mnt/arch/etc/fstab

Editer `/mnt/arch/etc/fstab` et modifier le point de montage de `/home`

## Chroot :

    arch-chroot /mnt/arch

## Configuration du système de base :

    echo "hostname" > /etc/hostname

Editer `/etc/locale.gen` et décommenter les lignes `fr_FR.UTF-8 UTF-8` et `fr_FR ISO-8859-1`
    
    locale-gen
    echo LANG=fr_FR.UTF-8 > /etc/locale.conf
    export LANG=fr_FR.UTF-8
    echo KEYMAP=fr-pc > /etc/vconsole.conf
    
    rm /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
    hwclock --systohc --utc
    
## Configuration de pacman :
    
Editer `/etc/pacman.conf` et activer le dépôt `multilib` 

    pacman -Syu

## Configuration de yay :

    git clone https://aur.archlinux.org/yay.git
    cd yay 
    makepkg -si
    
## Configuration de make pour compiler en utilisant 8 coeurs (sur 12) :

    htop #Vérifier le nombre de coeurs disponibles
    vim /etc/make.conf
    MAKEOPTS='-j8'
    MAKEFLAGS='-j8'

## Configuration des utilisateurs:

    passwd #mot de passe root

    useradd -mg users -G wheel,storage,power -s /bin/zsh user
    passwd user
    
    mount /dev/sda3 /home
    mkdir /home/user
    chown user /home/user
    
    pacman -S sudo vim
    visudo

Ajouter la ligne : `%wheel ALL=(ALL) ALL`

## Installation des paquets de base :

    pacman -S ntfs-3g dialog wpa_supplicant xorg-server xorg-apps git wget pulseaudio-alsa alsa-utils vim unrar p7zip networkmanager network-manager-applet htop hplip
    pacman -S chromium firefox firefox-i18n-fr nautilus file-roller zsh terminator feh keepass xdotools
    
## Installation de Cinnamon :

    pacman -S cinnamon cinnamon-translations gnome-terminal gnome-extra lightdm-gtk-greeter shotwell rhythmbox system-config-printer numlockx
    pacman -S lightdm-gtk-greeter-settings
    
## Installation et configuration de grub :

    pacman -S grub os-prober efibootmgrdosfstools mtools 
    mkdir -p /boot/EFI/arch
    mount /dev/nvme0n1p1 /boot/EFI
    grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
    grub-mkconfig -o /boot/grub/grub.cfg
    
## Post-installation :

    localectl set-keymap fr-pc #Si pas fait pendant l'install
    systemctl start lightdm
    systemctl enable lightdm
    systemctl start NetworkManager
    systemctl enable NetworkManager
    
Editer le fichier `/etc/lightdm/lightdm.conf` et ajouter cette ligne dans la rubrique `[Seat:*]` :

    greeter-setup-script=/usr/bin/numlockx on
    
## Utilisation de pacman :

    pacman -Syu #mettre à jour les dépôts
    pacman -S paquet #installer
    pacman -Rcs paquet #supprimer et purger
    
## Liens

- [Tuto TecMint](https://www.tecmint.com/arch-linux-installation-and-configuration-guide/)
- [Gitlab Edricus](https://gitlab.com/edricus/edri-archinstall/blob/master/install)
- [Github Fred Bezies (Cinnamon)](https://github.com/FredBezies/arch-tuto-installation/blob/master/install.md#viii-addendum-5-installer-cinnamon)
- [Partition EFI (ESP)](https://wiki.archlinux.fr/ESP)
- [Installation Guide](https://wiki.archlinux.org/index.php/installation_guide)
- [Installation de base](https://wiki.archlinux.fr/installation)